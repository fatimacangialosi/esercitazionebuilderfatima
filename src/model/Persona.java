package model;

import java.util.ArrayList;
import java.util.List;



public class Persona {
    private int id;
    private String nome;

    private String cognome;

    private int età;
    private String email;

    private String numeroDiTelefono;



    public Persona(int id, String nome, String cognome, int età, String email, String numeroDiTelefono
    ) {
        this.id = id;
        this.nome = nome;
        this.cognome = cognome;
        this.età = età;
        this.email = email;
        this.numeroDiTelefono = numeroDiTelefono;
    }


    @Override
    public String toString() {
        return "Persona{" +
                "id=" + id +
                ", nome='" + nome + '\'' +
                ", cognome='" + cognome + '\'' +
                ", età=" + età +
                ", email='" + email + '\'' +
                ", numeroDiTelefono='" + numeroDiTelefono + '\'' +
                '}';
    }

    public class GestorePersone {
        private List<Persona> persone;

        public GestorePersone() {
            this.persone = new ArrayList<>();
        }





        }


}
