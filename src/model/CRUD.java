package model;
 import java.util.List;
public interface CRUD<T> {
  //public Arraylist<>;
  //public void add<>;
     void add(T t) ;
    List<T> find();
    void update(T t);
    void delete(T t);

}

