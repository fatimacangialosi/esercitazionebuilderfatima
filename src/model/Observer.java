package model;
 package util.email;




    /**

     * Una classe può implementare l'interfaccia Observer quando vuole essere informata dei cambiamenti negli oggetti osservabili.
     */
    public interface Observer {
        /**
         * Questo metodo viene chiamato ogni volta che l'oggetto osservato viene cambiato.
         */
        void update();
}

