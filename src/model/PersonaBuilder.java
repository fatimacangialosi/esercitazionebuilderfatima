package model;

public class PersonaBuilder {
   private int id;

    private String nome;
    private String cognome;

    private int età;
   private String email;

   private String numeroDiTelefono;

    private PersonaBuilder(int id) {
        this.id = id;
    }
        public static PersonaBuilder newBuilder(int id) {
            return new PersonaBuilder(id);
        }
        public PersonaBuilder nome(String nome) {
            this.nome = nome;
            return this;
        }
        public PersonaBuilder cognome(String cognome) {
            this.cognome = cognome;
            return this;
        }
        public PersonaBuilder età(int età) {
            this.età = età;
            return this;
        }
    public PersonaBuilder email(String email) {
        this.email = email;
        return this;
    }
    public PersonaBuilder numeroDiTelefono(String numeroDiTelefono) {
        this.numeroDiTelefono = numeroDiTelefono;
        return this;
    }
    public Persona build() {
        return new Persona(id,nome,cognome, età,email, numeroDiTelefono);
    }




    public static void main(String[] args) {
        Persona gianmarco = PersonaBuilder.newBuilder(4)
                .nome("0000001")
                .cognome("PlutoSecondo")
                .età(19)
                .email("labrador")
                .numeroDiTelefono("4444444444")

                .build();

        System.out.println(gianmarco);

    }


};

