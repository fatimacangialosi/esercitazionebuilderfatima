package datasource;

import javax.swing.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
public class ConnectionMySql {
    private Connection connectData;

    public Connection getConnectData() {
        String url = "jdbc:mysql://localhost:3306/model.Persona";
        String username = "root";
        String password = "";
        //Connection connectData = null;
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            connectData = DriverManager.getConnection(url, username, password);


        } catch (Exception e) {
            System.out.println(e);
        }
        return connectData;
    }

//    /**
//     * Metodo che prende l'istanza della connessione con il database H2, univoca per tutta l'esecuzione del programma.
//     */
//    public static ConnectionMySql getInstance() {
//        return instance;
//    }

}

